-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 18, 2017 at 06:34 PM
-- Server version: 5.1.37
-- PHP Version: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `tokotoko`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_akun`
--

CREATE TABLE IF NOT EXISTS `tbl_akun` (
  `id_user` int(255) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_akun`
--

INSERT INTO `tbl_akun` (`id_user`, `email`, `username`, `password`, `level`) VALUES
(1, 'lala@gmail.com', 'lala', '1234', 'user'),
(2, 'admin@gmail.com', 'lala', '4321', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_barang`
--

CREATE TABLE IF NOT EXISTS `tbl_barang` (
  `id_barang` int(25) NOT NULL AUTO_INCREMENT,
  `id_kategori` int(25) NOT NULL,
  `nama_product` varchar(255) NOT NULL,
  `harga_product` varchar(255) NOT NULL,
  `kategori_product` varchar(25) NOT NULL,
  `kondisi_product` varchar(50) NOT NULL,
  `berat_product` varchar(50) NOT NULL,
  `pemesanan_min` varchar(50) NOT NULL,
  `deskripsi_product` text NOT NULL,
  `gambar_product` varchar(255) NOT NULL,
  `catatan_product` varchar(255) NOT NULL,
  PRIMARY KEY (`id_barang`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_barang`
--

INSERT INTO `tbl_barang` (`id_barang`, `id_kategori`, `nama_product`, `harga_product`, `kategori_product`, `kondisi_product`, `berat_product`, `pemesanan_min`, `deskripsi_product`, `gambar_product`, `catatan_product`) VALUES
(1, 1, 'Baju Isometric', '125000', 'Baju', 'Baru', '350gr', '1', 'Kaos oblong pria ini dirancang dengan desain simple dari bahan katun. Motif tipografi print yang cocok kamu kenakan untuk keseharian dengan harga terjangkau.\r\n\r\nWarna : Merah, Putih, Abu, Misty Muda\r\nSize : \r\nM : (LD : 50 cm ; PJ : 70 cm)\r\nL : (LD : 52 cm ; PJ : 72 cm)\r\nXL : (LD : 54 cm ; PJ : 74 cm)', 'product_11.jpg', ''),
(2, 2, 'PS 4 (Bonus 3 Game)', '3760000', 'Mainan / Hobi', 'Baru', '800gr', '1', 'Claim Garansi cukup dengan membawa nota pembelian dan box ps4. Karna Ps4 Yang kamu beli Sudah terdaftar Resmi di SONY ASIA Hari gini masih di ribetin sama kartu garansi, GAK ZAMAN Mulailah beralih ke yang lbih praktis..., simple dan gak ribet, apalagi yg bikin kantong bolong!! Features: - Garansi 1 tahun Sony Indonesia - Tersedia dalam 1 (satu) pilihan warna saja: Jet Black Package Include: - 1x Mesin Ps4 500gb CUH-2006A Jet Black - 1x DUALSHOCK4 Wireless Controller - 1x Mono Headset - 1x AC Power Cable - 1x HDMI Cable - 1x USB cable - 1x Manual Book SUPER GAMES WA: 0812 8897 9860, T0ko: 021- 601 9024 Jembatan NIaga 1 ITC Dusit Mangga Dua No. 25-27', 'product_09.png', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori`
--

CREATE TABLE IF NOT EXISTS `tbl_kategori` (
  `id_kategori` int(25) NOT NULL AUTO_INCREMENT,
  `kategori_product` varchar(25) NOT NULL,
  `gambar_kategori` varchar(255) NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tbl_kategori`
--

INSERT INTO `tbl_kategori` (`id_kategori`, `kategori_product`, `gambar_kategori`) VALUES
(1, 'Baju', 'Baju.jpg'),
(2, 'Mainan / Hobi', 'Mainan-Hobi.jpg'),
(3, 'Buku', 'Buku.jpg'),
(4, 'Kamera', 'Kamera.jpg'),
(5, 'Handphone', 'Handphone.jpg'),
(6, 'Musik', 'Musik.jpg'),
(7, 'Pc / Laptop', 'Pc-laptop.jpg'),
(8, 'Olahraga', 'Olaharaga.jpg'),
(9, 'Otomotif', 'Otomotif.jpg'),
(10, 'Kesehatan', 'Kesehatan.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nav`
--

CREATE TABLE IF NOT EXISTS `tbl_nav` (
  `id_nav` int(25) NOT NULL AUTO_INCREMENT,
  `nav_name` text NOT NULL,
  `link` varchar(25) NOT NULL,
  PRIMARY KEY (`id_nav`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_nav`
--

INSERT INTO `tbl_nav` (`id_nav`, `nav_name`, `link`) VALUES
(1, 'Contact', '#'),
(2, 'About', '#'),
(4, 'Home', '../index.php');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE IF NOT EXISTS `tbl_order` (
  `id_pembeli` int(50) NOT NULL AUTO_INCREMENT,
  `nama_pembeli` varchar(255) NOT NULL,
  `email_pembeli` varchar(255) NOT NULL,
  `alamat_lengkap` varchar(255) NOT NULL,
  `provinsi_pembeli` varchar(255) NOT NULL,
  `kecamatan_pembeli` varchar(255) NOT NULL,
  `kode_pos` varchar(255) NOT NULL,
  `kota_pembeli` varchar(255) NOT NULL,
  `nomor_telepon` varchar(255) NOT NULL,
  `catatan_pembeli` varchar(255) NOT NULL,
  `total_harga` varchar(255) NOT NULL,
  PRIMARY KEY (`id_pembeli`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_order`
--

