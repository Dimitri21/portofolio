
	<html>
	<head>
		<title> TOKOTOKO </title>
		<link rel="stylesheet" type="text/css" href="../assets/css/main.css">
	</head>
	<?php require_once '../class.php'; ?>
	<body>
		<div class="navbar">
			<div class="top">
				<div class="top-nav-container">
					<div class="top-nav">
						<span>
							<?php

							@session_start();
							if ($_SESSION['level']!='user') {
									include "login/login.php";
								}else{

								
						?>
						<div class="dropdown-user">
							<button class="dropbtn-user"><img src="assets/image/logo/user-logo.png" width="30px" height="auto"></button>
							<div class="dropdown-content-user">
								<div class="user-name">
									<p><?php echo $_SESSION['username']?></p>
								</div>
								<a href="#">Profile</a>
								<a href="view/login/logout.php">Logout</a>
							</div>
						</div>
						<?php } ?>
						</span>

						<?php
							$result = $db->fetch("SELECT * FROM tbl_nav");
							foreach ($result as $data) {
						?>
							<span>
							<a href="<?php echo $data['link'] ?>">
								<?php echo $data['nav_name']?>
							</a>
						<?php } ?>
							</span>
					</div>
				</div>
			</div>
			<div class="nav">
				<div class="nav-container">
					<div class="nav-logo">
						<a href="index.php">TOKOTOKO</a>
					</div>
					<div class="nav-search">
						<input type="text" name="search" placeholder="Cari disini..."></input>
					</div>
					<div class="dropdown">
						<button class="dropbtn"><span>&#9776;</span></button>
						<div class="dropdown-content">
							<?php 
							$result = $db->fetch("SELECT * FROM tbl_kategori");
							foreach ($result as $data) {
						?>
								<a href="page-product.php?kategori_product=<?php echo $data['kategori_product']?>">
								<?php echo $data['kategori_product'] ?></a>

							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="banner-shop-cart">
				<?php

				$result = $db->fetch("SELECT * FROM tbl_barang WHERE nama_product = '$_GET[product]'");
				foreach ($result as $data);

				?>
					<p><?php echo $data['nama_product']; ?></p>
			</div>
			<div class="divider"></div>
			<div class="side-bar">
				<h4 class="sidebar-kategori">KATEGORI</h4>
				<?php 
					$result = $db->fetch("SELECT * FROM tbl_kategori");
					foreach ($result as $data) {
				?>
				<li><a href="page-product.php?kategori_product=<?php echo $data['kategori_product']?>">
				<?php echo $data['kategori_product'] ?></a></li>
				<?php } ?>
				<div class="promo">
					<img src="../assets/image/promo/promo-1.jpg" width="100%">
				</div>
			</div>
			<?php

				$result = $db->fetch("SELECT * FROM tbl_barang WHERE nama_product = '$_GET[product]'");
				foreach ($result as $data);

			?>
			<div class="details-container">
				<div class="image-item">
					<img src="../assets/image/product/thumb/<?php echo $data['gambar_product']; ?>" width="100%" height="auto">
				</div>
				<div class="details-item">
					<div class="details-list">
					<div class="information-product">
						<h4>Informasi product.</h4>
					</div>
						<div class="col-information-product">
							<div class="col-img-small">
								<img src="../assets/image/logo/price-tags.png" width="100%" height="auto">
							</div>
							<p>Kondisi</p><span><?php echo $data['kondisi_product']; ?></span>
						</div>
						<div class="col-information-product right-col">
							<div class="col-img-small"> 
								<img src="../assets/image/logo/briefcase.png" width="100%" height="auto">
							</div>
							<p>Berat</p><span><?php echo $data['berat_product']; ?></span>
						</div>
						<div class="col-information-product">
							<div class="col-img-small">
								<img src="../assets/image/logo/cart.png" width="100%" height="auto">
							</div>
							<p>Pemesanan Min.</p><span><?php echo $data['pemesanan_min']; ?></span>
						</div>
						<div class="col-information-product right-col">
							<div class="col-img-small">
								<img src="../assets/image/logo/rp.png" width="100%" height="auto">
							</div>
							<p>Harga</p><span><?php echo $data['harga_product']; ?></span>
						</div>
					</div>
					<div class="col-big">
						<a href="detail-pembelian.php?barang=<?php echo $data['id_barang']; ?>">Beli Sekarang</a>
					</div>
					<div class="information-product deskripsi">
						<h4>Deskripsi Product</h4>
						<div class="description-product-container">
							<pre><p><?php echo $data['deskripsi_product'];?></pre>
						</div>
					</div>
				</div>
			</div>
			<div class="divider"></div>
			<div class="service-cotainer">
				<div class="service first">
					<img src="../assets/image/logo/service/secured.png" width="auto">
					<h3>Pembayaran <strong>mudah</strong> & <strong>aman</strong></h3>
				</div>
				<div class="service second">
					<img src="../assets/image/logo/service/best-quality.png" width="auto">
					<h3>Kualitas <strong>Terjamin</strong></h3>
				</div>
				<div class="service third third-img">
					<img src="../assets/image/logo/service/fast-delivery.png" class=""width="100%">
					<h3 class="third-service-text">Pengiriman <strong>Cepat</strong></h3>
				</div>
			</div>
			<div class="divider"></div>
		</div>
		<div class="footer-background">
				<div class="footer-container">
					<div class="footer-item">
						<h4><strong>Bantuan</strong></h4>
						<li><a href="">Pembayaran</a></li>
						<li><a href="">Pemesanan</a></li>
						<li><a href="">Tentang Kami</a></li>
						<li><a href="">Cara Berbelanja</a></li>
					</div>
					<div class="footer-item">
						<h4><strong>Info TOKOTOKO</strong></h4>
						<li><a href="">Tentang TOKOTOKO</a></li>
						<li><a href="">Kategori Product</a></li>
					</div>
					<div class="footer-item">
						<h4><strong>Bergabung Dengan Kami</strong></h4>
						<li><a href="">Login</a></li>
						<li><a href="">Register</a></li>
					</div>
					<div class="footer-item">
						<h4><strong>Ikuti Kami</strong></h4>
						<li><a href=""><img src="assets/image/logo/social/facebook.png"></a></li>
						<li><a href=""><img src="assets/image/logo/social/twitter.png"></a></li>
						<li><a href=""><img src="assets/image/logo/social/instagram.png"></a></li>
						<li><a href=""><img src="assets/image/logo/social/pinterest.png" class="img-clear"></a></li>
					</div>
					<div class="footer-item">
						<h4><strong>Subscribe kami </strong></h4>
						<li>Masukan Email anda untuk mengetahui promo terbaru dari kami</li>
						<li><input type="email" name="email" placeholder="Masukan email anda..."></input>
					</div>
				</div>
			</div>
	</body>
</html> 
