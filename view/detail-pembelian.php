
	<html>
	<head>
		<title> TOKOTOKO </title>
		<link rel="stylesheet" type="text/css" href="../assets/css/main.css">
	</head>
	<?php require_once '../class.php'; ?>
	<script type="text/javascript" src="assets/js/main.js"></script>
	<body>
		<div class="navbar">
			<div class="top">
				<div class="top-nav-container">
					<div class="top-nav">
						<span>
							<?php

							@session_start();
							if ($_SESSION['level']!='user') {
									include "login/login.php";
								}else{

								
						?>
						<div class="dropdown-user">
							<button class="dropbtn-user"><img src="assets/image/logo/user-logo.png" width="30px" height="auto"></button>
							<div class="dropdown-content-user">
								<div class="user-name">
									<p><?php echo $_SESSION['username']?></p>
								</div>
								<a href="#">Profile</a>
								<a href="view/login/logout.php">Logout</a>
							</div>
						</div>
						<?php } ?>
						</span>

						<?php
							$result = $db->fetch("SELECT * FROM tbl_nav");
							foreach ($result as $data) {
						?>
							<span>
							<a href="<?php echo $data['link'] ?>">
								<?php echo $data['nav_name']?>
							</a>
						<?php } ?>
							</span>
					</div>
				</div>
			</div>
			<div class="nav">
				<div class="nav-container">
					<div class="nav-logo">
						<a href="index.php">TOKOTOKO</a>
					</div>
					<div class="nav-search">
						<input type="text" name="search" placeholder="Cari disini..."></input>
					</div>
					<div class="dropdown">
						<button class="dropbtn"><span>&#9776;</span></button>
						<div class="dropdown-content">
							<?php 
							$result = $db->fetch("SELECT * FROM tbl_kategori");
							foreach ($result as $data) {
						?>
								<a href="page-product.php?kategori_product=<?php echo $data['kategori_product']?>">
								<?php echo $data['kategori_product'] ?></a>

							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="banner-shop-cart">
				<p>Detail Pembelian</p>
			</div>
			<div class="divider"></div>
			<div class="cart-container">
				<div class="left-cart-container">
				
					<div class="header-name-cart">
						<span>NAMA PRODUCT</span>
					</div>
					<div class="header-quantity">
						<span>QUANTITY</span>
					</div>
					<div class="header-total">
						<span>TOTAL</span>
					</div>
				<!-- item cart -->
				<?php
					$result = $db->fetch("SELECT * FROM tbl_barang WHERE id_barang = '$_GET[barang]'");
					foreach ($result as $data);
					$titleproduct = substr($data['nama_product'],0,45);
					
				?>
					<div class="product-cart">
					<item class="cart-item">
						<img src="../assets/image/product/thumb/<?php echo $data['gambar_product'] ?>" width="100%" height="auto">
						<div class="cart-item-title">
							<p><?php echo $titleproduct ?></p>
						</div>
					</item>
					</div>
					<div class="quantity-cart">
						<input type="number" onchange="totalHarga()" value="1" name="jumlah-barang" id="qty">
						</input>
						<span>x</span>
						<p><?php echo $data['harga_product'] ?></p>
						<input type="hidden" onchange="totalHarga()" id="harga" value="<?php echo $data['harga_product'] ?>"></input>
					</div>
					<div class="total-cart">
						<span>Rp</span><p id="hargaAwal" onchange="totalHarga()" name="total"></p>
					</div>
					<form action="">
					<div class="divider"></div>
						<div class="detail-pembeli">
							<label>Nama Pembeli</label>
							<input type="text"></input>
							<div class="col-half">
								<label>Email Pembeli</label>
								<input type="email" required="Wajib Diisi"></input>
								<h3> Alamat Pengiriman </h3>
								<label>Provinsi</label>
								<input type="text"></input>
								<label>Kecamatan</label>
								<input type="text"></input>
								<label>Alamat Lengkap</label>
								<input type="text"></input>
							</div>
							<div class="col-half right">
								<label>Nomor Telepon</label>
								<input type="phone"></input>
								<div class="clear-pengiriman"></div>
								<label>Kota / Kabupaten</label>
								<input type="text"></input>
								<label>Kode Pos</label>
								<input type="text"></input>
								<label>Catatan</label>
								<input type="text"></input>
							</div>
					</div>
				<!-- item cart -->
				</div>
				<div class="right-cart-container">
					<div class="subtotal">
						<li class="li-left"><strong>Subtotal :</strong></li>
						<li class="li-right"><strong id="subtotal"></strong></li>
						<div class="clear"></div>
						<li class="li-left">Discount</li>
						<li class="li-right">Rp 0</li>
						<div class="clear"></div>
						<li class="li-left">Biaya Pengiriman</li>
						<li class="li-right">Rp 30.000</li>
					</div>
					<div class="total-payment">
						<h4>Total</h4>
						<p onchange="totalHarga()" id="hargaAkhir"></p>
					</div>
					<a href="#" class="checkout">Checkout</a>
				</div>
			</div>
			</form>
			<div class="divider"></div>
			<div class="service-cotainer">
				<div class="service first">
					<img src="../assets/image/logo/service/secured.png" width="auto">
					<h3>Pembayaran <strong>mudah</strong> & <strong>aman</strong></h3>
				</div>
				<div class="service second">
					<img src="../assets/image/logo/service/best-quality.png" width="auto">
					<h3>Kualitas <strong>Terjamin</strong></h3>
				</div>
				<div class="service third third-img">
					<img src="../assets/image/logo/service/fast-delivery.png" class=""width="100%">
					<h3 class="third-service-text">Pengiriman <strong>Cepat</strong></h3>
				</div>
			</div>
			<div class="divider"></div>
		</div>
		<div class="footer-background">
				<div class="footer-container">
					<div class="footer-item">
						<h4><strong>Bantuan</strong></h4>
						<li><a href="">Pembayaran</a></li>
						<li><a href="">Pemesanan</a></li>
						<li><a href="">Tentang Kami</a></li>
						<li><a href="">Cara Berbelanja</a></li>
					</div>
					<div class="footer-item">
						<h4><strong>Info TOKOTOKO</strong></h4>
						<li><a href="">Tentang TOKOTOKO</a></li>
						<li><a href="">Kategori Product</a></li>
					</div>
					<div class="footer-item">
						<h4><strong>Bergabung Dengan Kami</strong></h4>
						<li><a href="">Login</a></li>
						<li><a href="">Register</a></li>
					</div>
					<div class="footer-item">
						<h4><strong>Ikuti Kami</strong></h4>
						<li><a href=""><img src="assets/image/logo/social/facebook.png"></a></li>
						<li><a href=""><img src="assets/image/logo/social/twitter.png"></a></li>
						<li><a href=""><img src="assets/image/logo/social/instagram.png"></a></li>
						<li><a href=""><img src="assets/image/logo/social/pinterest.png" class="img-clear"></a></li>
					</div>
					<div class="footer-item">
						<h4><strong>Subscribe kami </strong></h4>
						<li>Masukan Email anda untuk mengetahui promo terbaru dari kami</li>
						<li><input type="email" name="email" placeholder="Masukan email anda..."></input>
					</div>
				</div>
			</div>
	</body>
</html> 
