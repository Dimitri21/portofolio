
	<html>
	<head>
		<title> TOKOTOKO </title>
		<link rel="stylesheet" type="text/css" href="assets/css/main.css">
		<script type="text/javascript" src="assets/js/main.js"></script>
	</head>
	<?php require_once 'class.php'; ?>
	<body>
		<div class="navbar">
			<div class="top">
				<div class="top-nav-container">
					<div class="top-nav">
						<span>
							<?php

							@session_start();
							if ($_SESSION['level']!='user') {
									include "view/login/login.php";
								}else{

								
						?>
						<div class="dropdown-user">
							<button class="dropbtn-user"><img src="assets/image/logo/user-logo.png" width="30px" height="auto"></button>
							<div class="dropdown-content-user">
								<div class="user-name">
									<p><?php echo $_SESSION['username']?></p>
								</div>
								<a href="#">Profile</a>
								<a href="view/login/logout.php">Logout</a>
							</div>
						</div>
						<?php } ?>
						</span>

						<?php
							$result = $db->fetch("SELECT * FROM tbl_nav");
							foreach ($result as $data) {
						?>
							<span>
							<a href="<?php echo $data['link'] ?>">
								<?php echo $data['nav_name']?>
							</a>
						<?php } ?>
							</span>
					</div>
				</div>
			</div>
			<div class="nav">
				<div class="nav-container">
					<div class="nav-logo">
						<a href="index.php">TOKOTOKO</a>
					</div>
					<div class="nav-search">
						<input type="text" name="search" placeholder="Cari disini..."></input>
					</div>
					<div class="dropdown">
						<button class="dropbtn"><span>&#9776;</span></button>
						<div class="dropdown-content">
							<?php 
							$result = $db->fetch("SELECT * FROM tbl_kategori");
							foreach ($result as $data) {
						?>
								<a href="page-product.php?kategori_product=<?php echo $data['kategori_product']?>">
								<?php echo $data['kategori_product'] ?></a>

							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<a href="#" class="banner">
				<img src="assets/image/banner/banner-7.png" width="100%" height="auto">
			</a>
			<div class="divider"></div>
			<div class="product-container">
				<h1>Lastest Product</h1>
				<div class="box-container-product">
				<?php 
					$result = $db->fetch("SELECT * FROM tbl_barang");
					foreach ($result as $data) {
						$titleproduct = substr($data['nama_product'], 0,35)
				?>	
					<item class="product">
							<div class="img-product">
								<img src="assets/image/product/thumb/<?php echo $data['gambar_product']; ?>" width="100%" 
									 height="auto">
							</div>
							<div class="divider-product"></div>
							<div class="product-title">
								<a href="view/page-details.php?product=<?php echo $data['nama_product']; ?>">
								<?php echo $titleproduct ?></a>
							</div>
							<div class="product-price">
								<a href="view/page-details.php?product=<?php echo $data['nama_product']; ?>">
								Rp <?php echo $data['harga_product']; ?></a>
							</div>
							<div class="product-tag">
								<img src="assets/image/logo/price-tags.png" width="auto" height="auto">
								<a href="view/page-product.php?kategori_product=<?php echo $data['kategori_product'];?>">
								<?php echo $data['kategori_product']; ?></a>
							</div>
							<div class="product-buy">
								<a href="view/detail-pembelian.php?barang=<?php echo $data['id_barang'];; ?>">Beli Sekarang</a>
							</div>
					</item>
					<?php } ?>
				</div>	
			</div>
			<div class="divider"></div>
			<div class="product-container">
				<h1>Hot Product</h1>
				<div class="box-container-product">
				<?php 
					$result = $db->fetch("SELECT * FROM tbl_barang");
					foreach ($result as $data) {
						$titleproduct = substr($data['nama_product'], 0,35)
				?>	
					<item class="product">
							<div class="img-product">
								<img src="assets/image/product/thumb/<?php echo $data['gambar_product']; ?>" width="100%" 
									 height="auto">
							</div>
							<div class="divider-product"></div>
							<div class="product-title">
								<a href="view/page-details.php?product=<?php echo $data['nama_product']; ?>">
								<?php echo $titleproduct ?></a>
							</div>
							<div class="product-price">
								<a href="view/page-details.php?product=<?php echo $data['nama_product']; ?>">
								Rp <?php echo $data['harga_product']; ?></a>
							</div>
							<div class="product-tag">
								<img src="assets/image/logo/price-tags.png" width="auto" height="auto">
								<a href="view/page-product.php?kategori_product=<?php echo $data['kategori_product'];?>">
								<?php echo $data['kategori_product']; ?></a>
							</div>
							<div class="product-buy">
								<a href="view/detail-pembelian.php?barang=<?php echo $data['id_barang'];; ?>">Beli Sekarang</a>
							</div>
					</item>
					<?php } ?>
				</div>	
			</div>
			<div class="divider"></div>
			<div class="product-container">
				<h1>Kategori</h1>
				<div class="box-container-kategori">
				<?php
					$result = $db->fetch("SELECT * FROM tbl_kategori");
					foreach ($result as $data) {
				?>	
					<a href="view/page-product.php?kategori_product=<?php echo $data['kategori_product'];?>" 
					class="kategori kategori-logo">
						<img src="assets/image/logo/kategori-images/<?php echo $data['gambar_kategori'];?>" width="100%" height="auto">
						<div class="kategori-text"><p><?php echo $data['kategori_product']; ?></p></div>
					</a>
				<?php } ?>
				</div>
			</div>
			<div class="divider"></div>
			<div class="service-cotainer">
				<div class="service first">
					<img src="assets/image/logo/service/secured.png" width="auto">
					<h3>Pembayaran <strong>mudah</strong> & <strong>aman</strong></h3>
				</div>
				<div class="service second">
					<img src="assets/image/logo/service/best-quality.png" width="auto">
					<h3>Kualitas <strong>Terjamin</strong></h3>
				</div>
				<div class="service third third-img">
					<img src="assets/image/logo/service/fast-delivery.png" class=""width="100%">
					<h3 class="third-service-text">Pengiriman <strong>Cepat</strong></h3>
				</div>
			</div>
			<div class="divider"></div>
		</div>
		<div class="footer-background">
				<div class="footer-container">
					<div class="footer-item">
						<h4><strong>Bantuan</strong></h4>
						<li><a href="">Pembayaran</a></li>
						<li><a href="">Pemesanan</a></li>
						<li><a href="">Tentang Kami</a></li>
						<li><a href="">Cara Berbelanja</a></li>
					</div>
					<div class="footer-item">
						<h4><strong>Info TOKOTOKO</strong></h4>
						<li><a href="">Tentang TOKOTOKO</a></li>
						<li><a href="">Kategori Product</a></li>
					</div>
					<div class="footer-item">
						<h4><strong>Bergabung Dengan Kami</strong></h4>
						<li><?php

							@session_start();
							if (!isset($_SESSION['username'])) {
									include "view/login/login.php";
									echo "<li><a href='register.php'>Register</a></li>";
								}else{
								
						?></li>
						<li><a href="">Profil</a></li>
						<li><a href="view/login/logout.php">Logout</a></li>
						<?php } ?>
					</div>
					<div class="footer-item">
						<h4><strong>Ikuti Kami</strong></h4>
						<li><a href=""><img src="assets/image/logo/social/facebook.png"></a></li>
						<li><a href=""><img src="assets/image/logo/social/twitter.png"></a></li>
						<li><a href=""><img src="assets/image/logo/social/instagram.png"></a></li>
						<li><a href=""><img src="assets/image/logo/social/pinterest.png" class="img-clear"></a></li>
					</div>
					<div class="footer-item">
						<h4><strong>Subscribe kami </strong></h4>
						<li>Masukan Email anda untuk mengetahui promo terbaru dari kami</li>
						<li><input type="email" name="email" placeholder="Masukan email anda..."></input>
					</div>
				</div>
			</div>
	</body>
</html> 
