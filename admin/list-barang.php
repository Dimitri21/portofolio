<?php

		@session_start();
		if (!isset($_SESSION['level'])) {
			header('location:../index.php');
		}	
		if ($_SESSION['level']!="admin") {
			header('location:../index.php');
		}
								
?>

<!DOCTYPE html>
<html>
<head>
	<title>Admin - TOKOTOKO</title>
	<link rel="stylesheet" type="text/css" href="../assets/css/main.css">
	<?php require_once '../class.php';?>
</head>
<body>
	<div class="admin-main-container">
		<div class="side-bar-admin">
		<div class="admin-panel">
			<h1>Admin Panel</h1>
		</div>
		<div class="clear"></div>
			<button class="accordion">Manage Akun</button>
			<div class="panel">
			  <a href="list-akun.php">Table Akun</a>
			</div>
			<button class="accordion">Manage Barang</button>
			<div class="panel">
			  <a href="list-barang.php">Table Barang</a>
			</div>
			<button class="accordion">Manage Pesanan</button>
			<div class="panel">
			  <a href="list-order.php">Table Order</a>
			</div>
			<button class="accordion">Admin</button>
			<div class="panel">
			  <a href="edit-admin.php">Edit Profile</a>
			  <div class="clear"></div>
			  <a href="../view/login/logout.php">Logout</a>
			</div>
	</div>
	<div class="admin-container">
		<div class="admin-user">
			<h2>Welcome <?php echo $_SESSION['username'];?></h2>
			<p>Apa yang akan anda lakukan ?</p>
		</div>
		<div class="admin-option">
			<div class="admin-insert">
				<a href="tambah-barang.php">Tambah Barang</a>
				<a href="list-barang.php">List Barang</a>
				<a href="invoice.php">Invoice</a>
			</div>
		</div>
		<div class="admin-table">
			<table>
				<tr>
					<th>Select</th>
					<th>Id</th>
					<th>Product</th>
					<th>Harga</th>
					<th>Kategori</th>
					<th>Berat</th>
					<th>Min. Order</th>
					<th>Deskripsi</th>
					<th>Gambar Product</th>
					<th>Edit</th>
				</tr>
				<form action="action/delete.php" method="POST">
				<?php
					$result = $db->fetch("SELECT * FROM tbl_barang");
					foreach ($result as $data) {
						$deskripsi = substr($data['deskripsi_product'],0,40)
				?>
				<tr>
					<td><input type="checkbox" name="del[]" value="<?php echo $data[id_barang]?>"></input></td>
					<td><?php echo $data['id_barang']?></td>
					<td><?php echo $data['nama_product']?></td>
					<td><?php echo $data['harga_product']?></td>
					<td><?php echo $data['kategori_product']?></td>
					<td><?php echo $data['berat_product']?></td>
					<td><?php echo $data['pemesanan_min']?></td>
					<td><?php echo "$deskripsi..."?></td>
					<td><?php echo $data['gambar_product']?></td>
					<td><a href="edit.php">Edit</a></td>
				</tr>
				<?php
					}
				?>
			</table>
				<input type="submit" name="do" value="DELETE"></input>
			</form>
		</div>	
	</div>
	</div>
</body>
</html>

<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    }
}
</script>
