<?php

		@session_start();
		if (!isset($_SESSION['level'])) {
			header('location:../index.php');
		}	
		if ($_SESSION['level']!="admin") {
			header('location:../index.php');
		}
								
?>

<!DOCTYPE html>
<html>
<head>
	<title>Admin - TOKOTOKO</title>
	<link rel="stylesheet" type="text/css" href="../assets/css/main.css">
	<?php require_once '../class.php';?>
</head>
<body>
	<div class="admin-main-container">
		<div class="side-bar-admin">
		<div class="admin-panel">
			<h1>Admin Panel</h1>
		</div>
		 <div class="clear"></div>
			<button class="accordion">Manage Akun</button>
			<div class="panel">
			  <a href="list-akun.php">Table Akun</a>
			</div>
			<button class="accordion">Manage Barang</button>
			<div class="panel">
			  <a href="list-barang.php">Table Barang</a>
			</div>
			<button class="accordion">Manage Pesanan</button>
			<div class="panel">
			  <a href="list-order.php">Table Order</a>
			</div>
			<button class="accordion">Admin</button>
			<div class="panel">
			  <a href="edit-admin.php">Edit Profile</a>
			  <div class="clear"></div>
			  <a href="../view/login/logout.php">Logout</a>
			</div>
	</div>
	<div class="admin-container">
		<div class="admin-user">
			<h2>Welcome <?php echo $_SESSION['username'];?></h2>
			<p>Apa yang akan anda lakukan ?</p>
		</div>
		<div class="admin-option">
			<div class="admin-insert">
				<a href="tambah-barang.php">Tambah Barang</a>
				<a href="list-barang.php">List Barang</a>
				<a href="invoice.php">Invoice</a>
			</div>
		</div>
		<div class="admin-tambah-content">
			<div class="header-tambah">
				<h3>Tambah Barang</h3>
			</div>
			<form action="action/proses-tambah-barang.php" method="POST" enctype="multipart/form-data">
			<div class="col-third">
				<input type="hidden" name="id_barang"></input>
				<h3> Informasi Product </h3>
				<label>Nama Product</label>
				<input type="text" name= "nama_product" required></input>
				<label>Kategori Product</label>
				<input type="text" name= "kategori_product" required></input>
				<label>Ketegori Id</label>
				<input type="text" name= "id_kategori" required></input>
				<label>Berat Product</label>
				<input type="text" name= "berat_product" required></input>
			</div>
			<div class="col-third">
				<h3 style="color: white;">.</h3>
				<label>Harga Product</label>
				<input type="text" name= "harga_product" required></input>
				<label>Pemesanan Min.</label>
				<input type="text" name= "pemesanan_min" required></input>
				<label>Kodisi Product</label>
				<input type="text" name= "kondisi_product"required></input>
				<label>Catatan Product</label>
				<input type="text" name= "catatan_product"></input>
			</div>
			<div class="col-third">
				<h3> Detail Product </h3>
				<label>Deskripsi</label>
				<textarea name= "deskripsi_product"></textarea>
				<label>Foto Product</label>
				<input type="file" name= "gambar_product" required></input>
				<div class="col-large">
					<input type="submit" value="Tambah Barang"></input>
				</div>
			</div>
			
		</div>
	</div>	
	</div>
</body>
</html>

<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    }
}
</script>
