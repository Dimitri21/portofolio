<?php
	
	class Database{

		protected $db;

		public function __construct($username = "root", $password = "", $host = "localhost", $dbname = "tokotoko"){

			try {
				$this->db = new PDO("mysql:host={$host};dbname={$dbname};charset=utf8", $username, $password);
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
				
			} catch (Exception $e) {
				throw new Exception($e->getMessage());
				
			}
		}

		public function query($sql, $data = array()){

			try {

				$que = $this->db->prepare($sql);
				$que->execute($data);

				return $que->fetch();

			} catch (Exception $e) {

				throw new Exception($e->getMessage());
				
				
			}

		}

		public function fetch($sql, $data = array()){

			try {

				$ftch = $this->db->prepare($sql);
				$ftch->execute($data);

				return $ftch->fetchAll();

			} catch (Exception $e) {

				throw new Exception($e->getMessage());
				
				
			}

		}

		public function insert($sql, $data = array()){

		try {

				$stmt = $this->db->prepare($sql);
				$stmt->execute($data);

				return TRUE;

			} catch (Exception $e) {

				throw new Exception($e->getMessage());
				
				
			}

		}

		public function delete($sql, $data = array()){

			$this->insert($sql, $data);

		}
	}
